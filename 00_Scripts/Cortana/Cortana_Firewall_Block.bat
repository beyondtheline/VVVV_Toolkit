@echo off

:Ask
echo Moechtest du Cortana freischalten oder blockieren?


set INPUT=
set /P INPUT=(F/B): %=%

If /I "%INPUT%"=="b" goto block 
If /I "%INPUT%"=="f" goto free

echo Incorrect input & goto Ask



:block
echo Moechtest du Cortana wirklich mit der Windows Firewall Blockieren?
echo.

set INPUT=
set /P INPUT=(Y/N): %=%

If /I "%INPUT%"=="y" goto yesBlock 
If /I "%INPUT%"=="n" goto noBlock

echo Incorrect input & goto block


:yesBlock
netsh advfirewall firewall add rule name="Cortana Block Traffic" dir=out action=block program="%windir%\systemapps\Microsoft.Windows.Cortana_cw5n1h2txyewy\SearchUI.exe"enable=yes profile=any

echo Cortana wurde Blockiert.

timeout 3 /nobreak >NUL

exit

:noBlock
echo Zum Schliessen
pause
exit


:free
netsh advfirewall firewall delete rule name="Cortana Block Traffic"
pause

exit
@echo off

set /a fehler=0

echo Stopping update service if running...
net stop wuauserv >nul 2>&1

echo(
echo Removing registy keys...
reg delete "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" /f
set /a fehler = %fehler% + %errorlevel%

echo(
echo Setting startup type of update service to 'Manual'...
reg add "HKLM\SYSTEM\CurrentControlSet\Services\wuauserv" /v "Start" /t REG_DWORD /d 3 /f
set /a fehler = %fehler% + %errorlevel%

echo(
if %fehler% gtr 0 (
	echo ERROR: Failed to change registry
	pause > nul
) else (
	echo Done.
)
@echo off

set /a fehler=0

echo Stopping update service if running...
net stop wuauserv >nul 2>&1

echo(
echo Adding registy keys...
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" /v "WUServer" /t REG_SZ /d "http://localhost" /f
set /a fehler = %fehler% + %errorlevel%
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" /v "WUStatusServer" /t REG_SZ /d "http://localhost" /f
set /a fehler = %fehler% + %errorlevel%
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v "NoAutoUpdate" /t REG_DWORD /d 1 /f
set /a fehler = %fehler% + %errorlevel%
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v "AUOptions" /t REG_DWORD /d 1 /f
set /a fehler = %fehler% + %errorlevel%
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v "UseWUServer" /t REG_DWORD /d 1 /f
set /a fehler = %fehler% + %errorlevel%

echo(
echo Setting startup type of update service to 'Disabled'...
reg add "HKLM\SYSTEM\CurrentControlSet\Services\wuauserv" /v "Start" /t REG_DWORD /d 4 /f
set /a fehler + %errorlevel%

echo(
if %fehler% gtr 0 (
	echo ERROR: Failed to change registry
	pause > nul
) else (
	echo Done.
)

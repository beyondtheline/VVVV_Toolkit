
New-Item -ItemType directory -Path $PSScriptRoot\DirectX_June_2010_Redistributable

Clear-Host
Write-Host This is the Downloader for DirectX June 2010 Redistributable
Write-Host `n
Write-Host starting download DirectX June 2010 Redistributable

$webClient = New-Object System.Net.WebClient
$webURL = "https://download.microsoft.com/download/8/4/A/84A35BF1-DAFE-4AE8-82AF-AD2AE20B6B14/directx_Jun2010_redist.exe"
$filePath = "$PSScriptRoot\DirectX_June_2010_Redistributable\directx_Jun2010_redist.exe\"
$webclient.DownloadFile($webURL,$filePath)


Write-Host `n
Write-Host Finished Downloading the DirectX June 2010 Redistributable




Start-Sleep -s 2
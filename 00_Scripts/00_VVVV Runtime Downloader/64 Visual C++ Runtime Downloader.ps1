
New-Item -ItemType directory -Path $PSScriptRoot\64_Visual_C++_Runtime

Clear-Host

Write-Host This is the Downloader for Visual C++ Runtime for VVVV 64 Bit
Write-Host `n
Write-Host starting download Visual C++ Runtime 2017 

$webClient = New-Object System.Net.WebClient
$webURL = "https://download.visualstudio.microsoft.com/download/pr/11100230/15ccb3f02745c7b206ad10373cbca89b/VC_redist.x64.exe"
$filePath = "$PSScriptRoot\64_Visual_C++_Runtime\Visual C++ Runtime 2017.exe\"
$webclient.DownloadFile($webURL,$filePath)


Write-Host `n
Write-Host starting download Visual C++ Runtime 2013

$webClient = New-Object System.Net.WebClient
$webURL = "http://download.microsoft.com/download/2/E/6/2E61CFA4-993B-4DD4-91DA-3737CD5CD6E3/vcredist_x64.exe"
$filePath = "$PSScriptRoot\64_Visual_C++_Runtime\Visual C++ Runtime 2013.exe\"
$webclient.DownloadFile($webURL,$filePath)


Write-Host `n
Write-Host starting download Visual C++ Runtime 2012

$webClient = New-Object System.Net.WebClient
$webURL = "https://download.microsoft.com/download/1/6/B/16B06F60-3B20-4FF2-B699-5E9B7962F9AE/VSU_4/vcredist_x64.exe"
$filePath = "$PSScriptRoot\64_Visual_C++_Runtime\Visual C++ Runtime 2012.exe\"
$webclient.DownloadFile($webURL,$filePath)


Write-Host `n
Write-Host starting download Visual C++ Runtime 2010

$webClient = New-Object System.Net.WebClient
$webURL = "https://download.microsoft.com/download/A/8/0/A80747C3-41BD-45DF-B505-E9710D2744E0/vcredist_x64.exe"
$filePath = "$PSScriptRoot\64_Visual_C++_Runtime\Visual C++ Runtime 2010.exe\"
$webclient.DownloadFile($webURL,$filePath)


Write-Host `n
Write-Host starting download Visual C++ Runtime 2008

$webClient = New-Object System.Net.WebClient
$webURL = "https://download.microsoft.com/download/2/d/6/2d61c766-107b-409d-8fba-c39e61ca08e8/vcredist_x64.exe"
$filePath = "$PSScriptRoot\64_Visual_C++_Runtime\Visual C++ Runtime 2008.exe\"
$webclient.DownloadFile($webURL,$filePath)

Write-Host `n
Write-Host Finished Downloading the Visual C++ Runtime for VVVV 64 Bit




Start-Sleep -s 2
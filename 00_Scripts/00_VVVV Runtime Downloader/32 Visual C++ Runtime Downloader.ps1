
New-Item -ItemType directory -Path $PSScriptRoot\32_Visual_C++_Runtime

Clear-Host
Write-Host This is the Downloader for Visual C++ Runtime for VVVV 32 Bit
Write-Host `n
Write-Host starting download Visual C++ Runtime 2017 

$webClient = New-Object System.Net.WebClient
$webURL = "https://download.visualstudio.microsoft.com/download/pr/11100229/78c1e864d806e36f6035d80a0e80399e/VC_redist.x86.exe"
$filePath = "$PSScriptRoot\32_Visual_C++_Runtime\Visual C++ Runtime 2017.exe\"
$webclient.DownloadFile($webURL,$filePath)


Write-Host `n
Write-Host starting download Visual C++ Runtime 2013

$webClient = New-Object System.Net.WebClient
$webURL = "http://download.microsoft.com/download/2/E/6/2E61CFA4-993B-4DD4-91DA-3737CD5CD6E3/vcredist_x86.exe"
$filePath = "$PSScriptRoot\32_Visual_C++_Runtime\Visual C++ Runtime 2013.exe\"
$webclient.DownloadFile($webURL,$filePath)


Write-Host `n
Write-Host starting download Visual C++ Runtime 2012

$webClient = New-Object System.Net.WebClient
$webURL = "https://download.microsoft.com/download/1/6/B/16B06F60-3B20-4FF2-B699-5E9B7962F9AE/VSU_4/vcredist_x86.exe"
$filePath = "$PSScriptRoot\32_Visual_C++_Runtime\Visual C++ Runtime 2012.exe\"
$webclient.DownloadFile($webURL,$filePath)


Write-Host `n
Write-Host starting download Visual C++ Runtime 2010

$webClient = New-Object System.Net.WebClient
$webURL = "https://download.microsoft.com/download/C/6/D/C6D0FD4E-9E53-4897-9B91-836EBA2AACD3/vcredist_x86.exe"
$filePath = "$PSScriptRoot\32_Visual_C++_Runtime\Visual C++ Runtime 2010.exe\"
$webclient.DownloadFile($webURL,$filePath)


Write-Host `n
Write-Host starting download Visual C++ Runtime 2008

$webClient = New-Object System.Net.WebClient
$webURL = "https://download.microsoft.com/download/d/d/9/dd9a82d0-52ef-40db-8dab-795376989c03/vcredist_x86.exe"
$filePath = "$PSScriptRoot\32_Visual_C++_Runtime\Visual C++ Runtime 2008.exe\"
$webclient.DownloadFile($webURL,$filePath)

Write-Host `n
Write-Host Finished Downloading the Visual C++ Runtime for VVVV 32 Bit




Start-Sleep -s 2
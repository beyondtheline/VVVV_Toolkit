# ImageMagix Text to Texture Module

How to install
-------------------------

* Download Repro : git@git.meso.net:vvvv/imagemagixtexttexture.git
* Install ImageMagick-$Version$-x64-dll.exe from Thirdparty folder

* Install Font from Folder \Assets\PatchAssets\BMW_Group_fonts
* Start Patch
* Check if the path to the convert.exe in the install ImageMagix Folder is right.
* Download latest Conten.json from meso.io
* Hit STRG+F6 to render the Fonts.
* Check if all Texts are rendered.

FAQ
------------------------

* How to finde the font names and write to file
.\convert.exe -list font | Out-File C:\Users\phlegma\Desktop\fonts.txt


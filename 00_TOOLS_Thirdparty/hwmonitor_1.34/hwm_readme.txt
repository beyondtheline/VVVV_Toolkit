
HWMonitor Readme file
----------------------

Version 1.34
December 2017
Contact : cpuz@cpuid.com
Web page: http://www.cpuid.com/softwares/hwmonitor.html
CPUID SDK : http://www.cpuid-pro.com/products-system-monitoring-kit.php

History
-------

--------------------------------------------------------------------------------------------------
1.34 - December 2017
- AMD Raven Ridge processors.
- Windows 10 Build 16299.
- Improved NVIDIA GPUs monitoring.

--------------------------------------------------------------------------------------------------
1.33 - October 2017
- Intel Coffee Lake processors and Z370 platform.
- Intel Skylake-X HCC processors.
- Intel Xeon Skylake-SP and Xeon W Skylake processors.

--------------------------------------------------------------------------------------------------
1.32 - August 2017
- Intel Core X processors (KBL-X and SKL-X) and X299 platform.
- AMD ThreadRipper and X399 platform.

--------------------------------------------------------------------------------------------------
1.31 - March 2017
- AMD Ryzen processors.
- AMD Polaris GPU power report.

--------------------------------------------------------------------------------------------------
1.30 - October 2016
- Corsair Hydro series CPU coolers (H80i, H100i, H110i, H115i) support.
- Corsair RMi and AXi series PSUs support.
- NVMe SSDs support.
- Intel Kaby Lake processors.
- AMD Bristol Ridge processors.
- NVIDIA Pascal GPUs (GTX10x0).

--------------------------------------------------------------------------------------------------
1.29 - June 2016
- Intel Broadwell-E/EP processors.
- Intel Skylake Pentium and Celeron.
- AMD Carrizo APUs.
- Adds disks volumes space utilisation.

--------------------------------------------------------------------------------------------------
1.28 - July 2015
- Intel Broadwell and Intel Skylake CPUs.
- Added indivudual CPU load.
- Added NVIDIA TDP percentage

--------------------------------------------------------------------------------------------------
1.27 - March 2015
- Report CPU and GPU clocks.
- Intel Core M CPUs and preliminary support of Intel Skylake.

--------------------------------------------------------------------------------------------------
1.26 - December 2014
- Added CPU and GPU utilization.
- Added DRAM power (Haswell processors).
- Intel X99 Platform (DDR4 and Haswell-E).
- Support for Windows 10.
- New application icon.

--------------------------------------------------------------------------------------------------
1.25 - May 2014
- Intel Haswell-E, Core i7-4770R and Core i5-4570R Crystal Well, Celeron Haswell (G1830, G1820).
- AMD Athlon 5350 & 5150, Sempron 3850 & 2650 (Kabini), A10-7850K, A10-7800, A10-7700K, A8-7600, A6-7400K, A4-7300 (Kaveri), A6-6420K, A4-6320, A4-4020 (Richland).
- Nuvoton NCT6106 and SMSC SCH5636 SIOs (Fujitsu mainboards).
